module.exports = {
  apps: [
    {
      name: 'TitaniumThread',
      script: './dist/apps/api/main.js',
      watch: true,
      env: {
        HOST: '0.0.0.0',
        PORT: 80
      }
    }
  ]
};
