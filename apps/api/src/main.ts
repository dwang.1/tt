import { NestFactory } from '@nestjs/core';
import * as cors from 'cors';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cors({ origin: true }));
  const globalPrefix = 'api/v1';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3333;
  const host = process.env.HOST || 'localhost';
  await app.listen(port, host);
  console.log(`Listening at ${host}:${port}/${globalPrefix}`);
}

bootstrap();
