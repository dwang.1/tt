import { Body, Controller, Get, HttpService, UseInterceptors } from '@nestjs/common';

import { AuthTokenInterceptor } from './auth-token.interceptor';

@Controller('admin')
export class AdminController {
  constructor(private httpService: HttpService) {}

  @Get('accounts')
  @UseInterceptors(AuthTokenInterceptor)
  public async findAllAccounts(@Body() body: any) {
    const headersRequest = {
      'Content-Type': 'application/json',
      Authorization: `Basic ${body.token}`
    };

    return this.httpService.get('https://c0dieg1c93.execute-api.ap-southeast-2.amazonaws.com/dev/companies', { headers: headersRequest });
    // return 'test';
  }
}
