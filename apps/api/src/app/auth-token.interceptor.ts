import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthTokenInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Observable<any> {
    const request = context.switchToHttp().getRequest();
    console.log(request.headers.authorization.split(' ')[1]);
    request.body['token'] = request.headers.authorization.split(' ')[1];
    return next.handle();
  }
}
