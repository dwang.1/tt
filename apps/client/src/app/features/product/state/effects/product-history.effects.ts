import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Product } from '@tt/api-interfaces';
import { DataService } from '@tt/client/@core/services/data.service';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ProductHistoryActions } from '../actions';

@Injectable()
export class ProductHistoryEffects {
  constructor(private actions$: Actions, private service: DataService) {}

  loadHistory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductHistoryActions.loadProductsHistory),
      switchMap((user) =>
        this.service.getProducts(user as any).pipe(
          map((products: Product[]) => ProductHistoryActions.loadProductsHistorySuccess({ products })),
          catchError((error) => of(ProductHistoryActions.loadProductsHistoryFailure({ error })))
        )
      )
    )
  );
}
