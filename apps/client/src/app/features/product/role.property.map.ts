export const RolePropertyMap = {
  GROWER: [
    { field: 'companyName', header: 'Company Name' },
    { field: 'vineyardAddress', header: 'Vineyard Address' },
    { field: 'pickingDay', header: 'Picking Day' },
    { field: 'rowRange', header: 'Row Range' },
    { field: 'numberOfBins', header: 'Number Bins' },
    { field: 'block', header: 'Block' },
    { field: 'region', header: 'Region' },
    { field: 'vintage', header: 'Vintage' },
    { field: 'variety', header: 'Vriety' }
  ],
  LOGISTICS: [''],
  WINERY: [
    { field: 'actualWeight', header: 'Actual Weight' },
    { field: 'batchId', header: 'Batch Id' },
    { field: 'volume', header: 'Volume' },
    { field: 'status', header: 'Status' },
    { field: 'wineInfo', header: 'Wine Info' }
  ],
  BOTTLING: [''],
  STORAGE: [''],
  DISTRIBUTOR: ['']
};

export const FullProperySet = [
  { field: 'companyName', header: 'Company Name' },
  { field: 'vineyardAddress', header: 'Vineyard Address' },
  { field: 'pickingDay', header: 'Picking Day' },
  { field: 'rowRange', header: 'Row Range' },
  { field: 'numberOfBins', header: 'Number Bins' },
  { field: 'block', header: 'Block' },
  { field: 'region', header: 'Region' },
  { field: 'vintage', header: 'Vintage' },
  { field: 'variety', header: 'Vriety' },
  { field: 'actualWeight', header: 'Actual Weight' },
  { field: 'batchId', header: 'Batch Id' },
  { field: 'volume', header: 'Volume' },
  { field: 'status', header: 'Status' },
  { field: 'wineInfo', header: 'Wine Info' }
];
